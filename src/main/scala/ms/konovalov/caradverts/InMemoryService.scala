package ms.konovalov.caradverts

import java.util.UUID

import akka.actor.Status.Status
import akka.actor.{Actor, ActorLogging, ActorSystem, Props, Status}
import akka.pattern.ask
import akka.util.Timeout

import scala.concurrent.duration.DurationDouble
import scala.concurrent.{ExecutionContext, Future}
import scala.language.postfixOps

class InMemoryService(system: ActorSystem)(implicit val timeout: Timeout = Timeout(5 seconds), ec: ExecutionContext) extends Service {

  private val storage = system.actorOf(StorageActor.prop())

  override def create(advert: CreateAdvertDTO): Future[AdvertDTO] = {
    (storage ? advert).mapTo[Advert].map(toDTO)
  }

  override def get(id: UUID): Future[AdvertDTO] = {
    (storage ? GetRequest(id)).mapTo[Advert].map(toDTO)
  }

  override def delete(id: UUID): Future[Unit] = {
    (storage ? DeleteRequest(id)).map(_ => Unit)
  }

  override def list(page: Int, perPage: Int, sort: Option[String]): Future[AdvertsDTOList] = {
    (storage ? ListRequest(page, perPage, sort)).mapTo[AdvertsList].map(toAdvertsDTOList)
  }

  override def update(id: UUID, advert: AdvertDTO): Future[AdvertDTO] = {
    if (!id.equals(advert.id)) {
      throw UniqueIdViolation(id)
    }
    (storage ? advert).mapTo[Advert].map(toDTO)
  }
}

private case class GetRequest(id: UUID)

private case class DeleteRequest(id: UUID)

private case class ListRequest(page: Int, perPage: Int, sort: Option[String])

private object StorageActor {
  def prop(): Props = Props(classOf[StorageActor])
}

private class StorageActor() extends Actor with ActorLogging {

  private var storage: Map[UUID, Advert] = Map.empty

  override def receive: Receive = {

    case create: CreateAdvertDTO =>
      val advert = fromDTO(create)
      storage += (advert.id -> advert)
      sender() ! Status.Success(advert)

    case GetRequest(id) =>
      sender() ! storage.get(id).map(Status.Success(_)).getOrElse(Status.Failure(AdvertNotFound(id)))

    case DeleteRequest(id) =>
      storage -= id
      sender() ! Status.Success()

    case ListRequest(page, perPage, sort) =>
      val totalRecords = storage.size
      val result = AdvertsList(page = page,
        totalPages = (totalRecords + perPage - 1) / perPage,
        count = totalRecords,
        adverts = storage.values.toSeq.sortBy(adv => getFieldValueByName(adv, sort)).drop((page - 1) * perPage).take(perPage)
      )
      sender() ! Status.Success(result)

    case update: AdvertDTO =>
      val response: Status = storage.get(update.id).map(old => {
        if (old.version != update.version) {
          Status.Failure(ConcurrentModification(update.id))
        } else {
          val advert = fromDTO(update.copy(version = update.version + 1))
          storage = storage.updated(update.id, advert)
          Status.Success(advert)
        }
      }).getOrElse(Status.Failure(AdvertNotFound(update.id)))
      sender() ! response
  }

  private def getFieldValueByName(obj: Advert, field: Option[String]): String =
    field.flatMap(f => obj.getClass.getDeclaredFields.map(_.getName).zip(obj.productIterator.to).toMap.get(f)).getOrElse(obj.id).toString

}