package ms.konovalov

import java.time.LocalDate
import java.util.UUID

package object caradverts {

  def validate(title: String, price: Int, isNew: Boolean, mileage: Option[Int], firstRegistration: Option[LocalDate]): Unit = {
    require(!title.isEmpty, "title must not be empty")
    require(price > 0, "price must be positive")
    require(isNew || mileage.isDefined, "mileage should be filled for used car")
    require(mileage.isEmpty || mileage.get > 0, "mileage must be positive")
    require(isNew || firstRegistration.isDefined, "date of first registration should be filled for used car")
    require(firstRegistration.isEmpty || firstRegistration.get.isBefore(LocalDate.now()), "first registration date must be in past")
  }

  def fromDTO(dto: CreateAdvertDTO): Advert =
    Advert(
      UUID.randomUUID(),
      0,
      dto.title,
      dto.fuel,
      dto.price,
      dto.isNew,
      dto.mileage,
      dto.firstRegistration
    )

  def fromDTO(dto: AdvertDTO): Advert =
    Advert(
      dto.id,
      dto.version,
      dto.title,
      dto.fuel,
      dto.price,
      dto.isNew,
      dto.mileage,
      dto.firstRegistration
    )

  def toDTO(dto: Advert): AdvertDTO =
    AdvertDTO(
      dto.id,
      dto.version,
      dto.title,
      dto.fuel,
      dto.price,
      dto.isNew,
      dto.mileage,
      dto.firstRegistration
    )

  def toAdvertsDTOList(list: AdvertsList): AdvertsDTOList = {
    AdvertsDTOList(
      page = list.page,
      totalPages = list.totalPages,
      count = list.count,
      adverts = list.adverts.map(adv => toDTO(adv))
    )
  }
}
