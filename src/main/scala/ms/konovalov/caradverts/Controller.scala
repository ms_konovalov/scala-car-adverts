package ms.konovalov.caradverts

import java.util.UUID

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.marshalling.Marshal
import akka.http.scaladsl.model.StatusCodes._
import akka.http.scaladsl.model.{HttpResponse, ResponseEntity, StatusCode}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.{ExceptionHandler, Route}
import akka.stream.ActorMaterializer
import akka.util.Timeout

import scala.concurrent.ExecutionContext
import scala.concurrent.duration.DurationInt
import scala.io.StdIn
import scala.language.postfixOps

class Controller(service: Service)(implicit am: ActorMaterializer, ec: ExecutionContext, system: ActorSystem)
  extends Protocol {

  implicit val timeout = Timeout(5 seconds)

  val exceptionHandler = ExceptionHandler {
    case e: IllegalArgumentException =>
      system.log.error(e, e.getMessage)
      convertError(BadRequest, Error(e.getMessage))
    case e: AdvertNotFound =>
      convertError(NotFound, Error(e.getMessage))
    case e: ConcurrentModification =>
      convertError(Conflict, Error(e.getMessage))
    case e: UniqueIdViolation =>
      convertError(BadRequest, Error(e.getMessage))
    case e =>
      system.log.error(e, "Unexpected error")
      convertError(ServiceUnavailable, Error("Operation failed"))
  }

  val apiRoute: Route =
    handleExceptions(exceptionHandler) {
      pathPrefix("advert") {
        pathPrefix("api" / "v1") {
          pathEnd {
            post {
              entity(as[CreateAdvertDTO]) { data =>
                logRequest("POST advert") {
                  complete(service.create(data))
                }
              }
            } ~
              get {
                parameters('page.as[Int] ? 1, 'perPage.as[Int] ? 10, 'sort.?) { (page, perPage, sort) =>
                  logRequest(s"LIST adverts with page=$page perPage=$perPage sort=$sort") {
                    complete(service.list(page, perPage, sort))
                  }
                }
              }
          } ~
            path(JavaUUID) { id =>
              get {
                logRequest(s"GET advert with $id") {
                  complete(service.get(id))
                }
              } ~
                delete {
                  logRequest(s"DELETE advert with $id") {
                    onComplete(service.delete(id)) { result =>
                      complete(NoContent)
                    }
                  }
                } ~
                put {
                  entity(as[AdvertDTO]) { data =>
                    logRequest(s"PUT advert with $id") {
                      complete(service.update(id, data))
                    }
                  }
                }
            }
        }
      }
    }

  private def convertError(status: StatusCode, error: Error) = {
    onSuccess(Marshal(error).to[ResponseEntity]) { entity =>
      complete {
        HttpResponse(status = status, entity = entity)
      }
    }
  }
}

/** Class with error representation
  *
  * @param error error message
  */
case class Error(error: String)


case class Config(host: String = "0.0.0.0", port: Int = 8080, consoleRun: Boolean = true, mongoUri: String = "mongodb://localhost:27017/mydb?authMode=scram-sha1", mem: Boolean = false)

case object Parser extends scopt.OptionParser[Config]("") {

  opt[String]("mongouri").action((mongoUri, cfg) =>
    cfg.copy(mongoUri = mongoUri)).text("connection string to mongo db")

  opt[String]("host").action((host, cfg) =>
    cfg.copy(host = host)).text("host to listen requests, default 0.0.0.0")

  opt[Int]("port").action((port, cfg) =>
    cfg.copy(port = port)).text("http port to listen, default 8080")

  opt[Boolean]("console").action((cons, cfg) =>
    cfg.copy(consoleRun = cons)).text("start with console input, default true")

  opt[Boolean]("mem").action((mem, cfg) =>
    cfg.copy(mem = mem)).text("use in-memory db, default false")

  help("help")

  override def showUsageOnError = true
}

/** Main entry point
  */
object Main extends App {

  private val cfg: Config = Parser.parse(args, Config()).getOrElse {
    Parser.showUsage()
    sys.exit()
  }

  implicit val system = ActorSystem("my-system")
  implicit val materializer = ActorMaterializer()
  implicit val ec = system.dispatcher

  val service = if (cfg.mem) new InMemoryService(system) else new MongoService(cfg.mongoUri)
  val bindingFuture = Http().bindAndHandle(new Controller(service).apiRoute, cfg.host, cfg.port)

  println(s"Server started on interface ${cfg.host} and port ${cfg.port}")

  sys.addShutdownHook(shutdown())

  if (cfg.consoleRun) {
    println("Press any key to exit...")
    StdIn.readLine()
    shutdown()
  }

  def shutdown(): Unit = {
    bindingFuture.flatMap(_.unbind()).onComplete(_ => system.terminate())
  }
}
