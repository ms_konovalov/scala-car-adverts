package ms.konovalov.caradverts

import java.time.LocalDate
import java.util.UUID

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.marshalling.{Marshaller, ToEntityMarshaller}
import akka.http.scaladsl.model.{HttpEntity, MediaTypes}
import ms.konovalov.caradverts.FuelType._
import spray.json.DefaultJsonProtocol

import scala.util.Try

trait Protocol extends SprayJsonSupport with DefaultJsonProtocol {

  import spray.json._

  implicit val errorFormat: RootJsonFormat[Error] = jsonFormat1(Error)

  implicit def errorResponseMarshaller: ToEntityMarshaller[Error] =
    Marshaller.withFixedContentType(MediaTypes.`application/json`) { error =>
      HttpEntity(error.toJson.compactPrint)
    }

  implicit val uuidFormat: RootJsonFormat[UUID] = new RootJsonFormat[UUID] {

    override def read(json: JsValue): UUID = json match {
      case s: JsString => UUID.fromString(s.value)
      case _ => deserializationError("Failed to parse json string [" + json + "].")
    }

    override def write(data: UUID): JsValue = JsString(data.toString)
  }

  implicit val fuelTypeFormat: RootJsonFormat[FuelType] = new RootJsonFormat[FuelType] {

    override def read(json: JsValue): FuelType = json match {
      case JsString(str) => FuelType.fromString(str).getOrElse(deserializationError("Expected FuelType as JsString, but got " + str))
      case _ => deserializationError("Failed to parse json string [" + json + "].")
    }

    override def write(data: FuelType): JsValue = JsString(data.name)
  }

  implicit val dateJsonFormat: RootJsonFormat[LocalDate] = new RootJsonFormat[LocalDate] {

    override def read(json: JsValue): LocalDate = json match {
      case JsString(value) => Try(LocalDate.parse(value)).getOrElse(deserializationError("Failed to parse date time [" + value + "]."))
      case _ => deserializationError("Failed to parse json string [" + json + "].")
    }

    override def write(data: LocalDate) = JsString(data.toString)
  }

  implicit val initAdvertFormat: RootJsonFormat[CreateAdvertDTO] = jsonFormat6(CreateAdvertDTO)

  implicit val advertFormat: RootJsonFormat[AdvertDTO] = jsonFormat8(AdvertDTO)

  implicit val listFormat: RootJsonFormat[AdvertsDTOList] = jsonFormat4(AdvertsDTOList)

}
