package ms.konovalov.caradverts

import java.time.LocalDate
import java.util.UUID

import akka.util.Timeout
import reactivemongo.api._
import reactivemongo.api.collections.bson.BSONCollection
import reactivemongo.bson._

import scala.concurrent.duration.DurationDouble
import scala.concurrent.{ExecutionContext, Future}
import scala.language.postfixOps

class MongoService(mongoUri: String)(implicit val timeout: Timeout = Timeout(5 seconds), ec: ExecutionContext) extends Service {

  private val driver = MongoDriver()
  private val futureConnection = Future.fromTry(MongoConnection.parseURI(mongoUri).map(driver.connection))

  def collection: Future[BSONCollection] = futureConnection.flatMap(_.database("db")).map(_.collection("adverts"))

  implicit object AdvertBSONReader extends BSONDocumentReader[Advert] {

    def read(doc: BSONDocument): Advert =
      Advert(
        id = UUID.fromString(doc.getAs[String]("_id").get),
        version = doc.getAs[Int]("version").get,
        title = doc.getAs[String]("title").get,
        fuel = FuelType.fromString(doc.getAs[String]("fuel").get).get,
        price = doc.getAs[Int]("price").get,
        isNew = doc.getAs[Boolean]("isNew").get,
        mileage = doc.getAs[Int]("mileage"),
        firstRegistration = doc.getAs[String]("firstRegistration").map(LocalDate.parse)
      )
  }

  implicit object AdvertEntityBSONWriter extends BSONDocumentWriter[Advert] {
    def write(advert: Advert): BSONDocument =
      BSONDocument(
        "_id" -> advert.id.toString,
        "version" -> advert.version,
        "title" -> advert.title,
        "fuel" -> advert.fuel.name,
        "price" -> advert.price,
        "isNew" -> advert.isNew,
        "mileage" -> advert.mileage,
        "firstRegistration" -> advert.firstRegistration.map(_.toString)
      )
  }

  override def create(input: CreateAdvertDTO): Future[AdvertDTO] = {
    val advert = fromDTO(input)
    collection.flatMap(_.insert(advert)).map(r => toDTO(advert))
  }

  override def get(id: UUID): Future[AdvertDTO] = {
    collection.flatMap(_.find(BSONDocument("_id" -> id.toString)).one[Advert]).map(result => result.map(toDTO).getOrElse(throw AdvertNotFound(id)))
  }

  override def delete(id: UUID): Future[Unit] = {
    collection.flatMap(_.remove(BSONDocument("_id" -> id.toString))).map(result => {})
  }

  private def count(): Future[Int] = {
    collection.flatMap(_.count())
  }

  override def list(page: Int, perPage: Int, sort: Option[String]): Future[AdvertsDTOList] = {
    for {
      totalRecords <- count()
      adverts <- collection.flatMap(_.find(BSONDocument())
        .sort(BSONDocument(sort.getOrElse("_id") -> 1))
        .options(QueryOpts(skipN = (page - 1) * perPage))
        .cursor[Advert]().collect[Seq](perPage, Cursor.FailOnError[Seq[Advert]]()))
    } yield toAdvertsDTOList(AdvertsList(page, (totalRecords + perPage - 1) / perPage, totalRecords, adverts))
  }

  override def update(id: UUID, advert: AdvertDTO): Future[AdvertDTO] = {
    if (!id.equals(advert.id)) {
      throw UniqueIdViolation(id)
    }

    val selector = BSONDocument(
      "_id" -> id.toString,
      "version" -> advert.version
    )

    val modifier = BSONDocument(
      "$set" -> fromDTO(advert)
    )

    get(id).flatMap(res =>
      collection.flatMap(_.findAndUpdate(selector, modifier, fetchNewObject = true)
        .map(_.result[Advert])
        .map(_.getOrElse(throw ConcurrentModification(id)))
        .map(toDTO)
      )
    )

  }
}