package ms.konovalov.caradverts

import java.time.LocalDate
import java.util.UUID

import ms.konovalov.caradverts.FuelType.FuelType

case class AdvertDTO(id: UUID,
                     version: Int,
                     title: String,
                     fuel: FuelType,
                     price: Int,
                     isNew: Boolean,
                     mileage: Option[Int] = None,
                     firstRegistration: Option[LocalDate] = None
                    ) {
  validate(title, price, isNew, mileage, firstRegistration)
}

case class CreateAdvertDTO(title: String,
                           fuel: FuelType,
                           price: Int,
                           isNew: Boolean,
                           mileage: Option[Int] = None,
                           firstRegistration: Option[LocalDate] = None
                          ) {
  validate(title, price, isNew, mileage, firstRegistration)
}

case class Advert(id: UUID,
                  version: Int,
                  title: String,
                  fuel: FuelType,
                  price: Int,
                  isNew: Boolean,
                  mileage: Option[Int] = None,
                  firstRegistration: Option[LocalDate] = None)

object FuelType {

  sealed abstract class FuelType(val name: String)

  case object G extends FuelType("gasoline")

  case object D extends FuelType("diesel")

  private val allTypes = Seq(G, D)

  def fromString(name: String): Option[FuelType] = allTypes.find(t => t.name.equals(name))
}

case class AdvertsList(page: Int,
                       totalPages: Int,
                       count: Int,
                       adverts: Seq[Advert])

case class AdvertsDTOList(page: Int,
                          totalPages: Int,
                          count: Int,
                          adverts: Seq[AdvertDTO])

case class AdvertNotFound(id: UUID) extends Exception(s"Advert not found with id=$id")

case class ConcurrentModification(id: UUID) extends Exception(s"Advert with id=$id is already modified")

case class UniqueIdViolation(id: UUID) extends Exception(s"Advert with id=$id already exists")
