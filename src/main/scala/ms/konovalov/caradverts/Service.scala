package ms.konovalov.caradverts

import java.util.UUID

import scala.concurrent.Future

trait Service {

  def create(advert: CreateAdvertDTO): Future[AdvertDTO]

  def get(id: UUID): Future[AdvertDTO]

  def delete(id: UUID): Future[Unit]

  def list(page: Int, perPage: Int, sort: Option[String]): Future[AdvertsDTOList]

  def update(id: UUID, advert: AdvertDTO): Future[AdvertDTO]
}