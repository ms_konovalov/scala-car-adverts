package ms.konovalov.caradverts

import akka.actor.ActorSystem
import akka.testkit.TestKit
import org.scalatest._

class InMemoryServiceTest extends AsyncFlatSpec
  with Matchers with BeforeAndAfterAll with BeforeAndAfterEach {

  val system = ActorSystem("MySpec")

  override def afterAll {
    TestKit.shutdownActorSystem(system)
  }

  private var service = new InMemoryService(system)

  override def beforeEach {
    service = new InMemoryService(system)
  }

  "Service" should "return empty list" in {
    service.list(1, 10, None) map (_.adverts shouldBe empty)
  }

  "Service" should "return list after adding" in {
    val input = Seq(createAdvertDTO(), createAdvertDTO())
    input.foreach(adv => service.create(adv))
    service.list(1, 10, Some("title")) map { list =>
      list.adverts should not be empty
      list.adverts.size shouldBe 2
      list.adverts.head shouldBe a[AdvertDTO]
    }
  }

  "Service" should "return list after adding with paging" in {
    val input = Seq(createAdvertDTO(), createAdvertDTO(), createAdvertDTO())
    input.foreach(adv => service.create(adv))
    service.list(2, 1, Some("title")) map { list =>
      list.adverts should not be empty
      list.adverts.size shouldBe 1
      list.adverts.head shouldBe a[AdvertDTO]
    }
  }

  "Service" should "be return added item" in {
    val input = createAdvertDTO()
    service.create(input) flatMap (r => {
      val id = r.id
      val result = service.get(id)
      result map (_.title shouldBe input.title)
    })
  }

  "Service" should "update advert" in {
    val input = createAdvertDTO()
    service.create(input) flatMap (r => {
      service.update(r.id, r.copy(price = 1))
      val result = service.get(r.id)
      result map (_.price shouldBe 1)
    })
  }

  "Service" should "delete advert" in {
    val input = createAdvertDTO()
    service.create(input) flatMap (r => {
      service.delete(r.id)
      recoverToSucceededIf[AdvertNotFound] {
        service.get(r.id)
      }
    })
  }

  def createAdvertDTO(): CreateAdvertDTO = CreateAdvertDTO(title = "title", fuel = FuelType.G, price = 100, isNew = true)

}
