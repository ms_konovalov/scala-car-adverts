package ms.konovalov.caradverts

import java.time.LocalDate
import java.util.UUID

import akka.http.scaladsl.model.StatusCodes._
import akka.http.scaladsl.model.{ContentType, HttpEntity, MediaTypes}
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.testkit.ScalatestRouteTest
import org.scalamock.scalatest.MockFactory
import org.scalatest._
import org.scalatest.matchers.Matcher

import scala.concurrent.Future

class ControllerTest extends WordSpec with Matchers with ScalatestRouteTest with Protocol
  with MockFactory {

  private val service = mock[Service]
  private val apiRoute = new Controller(service).apiRoute

  "API" should {

    "return a NotFound error for GET requests to different path" in {
      Get() ~> Route.seal(apiRoute) ~> check {
        status shouldEqual NotFound
      }
      Post("/bla") ~> Route.seal(apiRoute) ~> check {
        status shouldEqual NotFound
      }
    }

    "return a OK for POST requests to the /advert/api/v1" in {
      val input1 = CreateAdvertDTO("title", FuelType.G, 100, isNew = true)
      val result1 = convert(input1)
      (service.create _).expects(input1).noMoreThanOnce().returns(Future(result1))
      Post("/advert/api/v1", input1) ~> Route.seal(apiRoute) ~> check {
        status shouldEqual OK
        responseAs[AdvertDTO] should haveSameFields(result1)
      }
      val input2 = createEntity(isNew = true, mileage = None, firstRegistration = None)
      val result2 = AdvertDTO(UUID.randomUUID(), 0, "t", FuelType.G, 100, isNew = true)
      (service.create _).expects(*).noMoreThanOnce().returns(Future(result2))
      Post("/advert/api/v1", input2) ~> Route.seal(apiRoute) ~> check {
        status shouldEqual OK
        responseAs[AdvertDTO] should haveSameFields(result2)
      }
      val input3 = createEntity()
      val result3 = AdvertDTO(UUID.randomUUID(), 0, "t", FuelType.G, 100, isNew = false, Some(100), Some(LocalDate.now().minusDays(10)))
      (service.create _).expects(*).noMoreThanOnce().returns(Future(result3))
      Post("/advert/api/v1", input3) ~> Route.seal(apiRoute) ~> check {
        status shouldEqual OK
        responseAs[AdvertDTO] should haveSameFields(result3)
      }
    }

    "return a BadRequest for POST invalid requests to the /advert/api/v1" in {
      val args = Seq(
        HttpEntity(ContentType(MediaTypes.`application/json`), """{"aaa" : "aaa"}"""),
        HttpEntity(ContentType(MediaTypes.`application/json`), s"""{"id" : "${UUID.randomUUID}"}"""),
        createEntity(price = -100),
        createEntity(fuel = "vla"),
        createEntity(mileage = None),
        createEntity(firstRegistration = None),
        createEntity(mileage = Some(-100)),
        createEntity(firstRegistration = Some(LocalDate.now().plusDays(10)))
      )
      args.foreach(arg =>
        Post("/advert/api/v1", arg) ~> Route.seal(apiRoute) ~> check {
          status shouldEqual BadRequest
        }
      )
    }

    "return a OK for GET requests to the /advert/api/v1" in {
      val result = AdvertDTO(UUID.randomUUID(), 0, "t", FuelType.G, 100, isNew = true)
      (service.list _).expects(1, 10, None).noMoreThanOnce().returns(Future(AdvertsDTOList(1, 1, 1, Seq(result))))
      Get("/advert/api/v1") ~> Route.seal(apiRoute) ~> check {
        status shouldEqual OK
        responseAs[AdvertsDTOList].adverts should not be empty
        responseAs[AdvertsDTOList].adverts.head should haveSameFields(result)
      }
      (service.list _).expects(2, 7, Some("title")).noMoreThanOnce().returns(Future(AdvertsDTOList(2, 1, 1, Seq(result))))
      Get("/advert/api/v1?page=2&perPage=7&sort=title") ~> Route.seal(apiRoute) ~> check {
        status shouldEqual OK
        responseAs[AdvertsDTOList].adverts should not be empty
        responseAs[AdvertsDTOList].adverts.head should haveSameFields(result)
      }
    }

    "return a MethodNotAllowed for DELETE/PUT requests to the /advert" in {
      Delete("/advert/api/v1") ~> Route.seal(apiRoute) ~> check {
        status shouldEqual MethodNotAllowed
      }
      Put("/advert/api/v1") ~> Route.seal(apiRoute) ~> check {
        status shouldEqual MethodNotAllowed
      }
    }

    "return a NotFound for GET/DELETE/PUT requests to the /advert/api/v1/..." in {
      Get("/advert/api/v1/bla") ~> Route.seal(apiRoute) ~> check {
        status shouldEqual NotFound
      }
      Get("/advert/api/v1/12345") ~> Route.seal(apiRoute) ~> check {
        status shouldEqual NotFound
      }
      Delete("/advert/api/v1/bla") ~> Route.seal(apiRoute) ~> check {
        status shouldEqual NotFound
      }
      Delete("/advert/api/v1/12345") ~> Route.seal(apiRoute) ~> check {
        status shouldEqual NotFound
      }
      val advert = AdvertDTO(UUID.randomUUID(), 0, "title", FuelType.G, 100, isNew = true)
      Put("/advert/api/v1/bla", advert) ~> Route.seal(apiRoute) ~> check {
        status shouldEqual NotFound
      }
      Put("/advert/api/v1/12345", advert) ~> Route.seal(apiRoute) ~> check {
        status shouldEqual NotFound
      }
    }

    "return a OK for GET requests to the /advert/api/v1/UUID" in {
      val uuid = UUID.randomUUID()
      val result = AdvertDTO(uuid, 0, "t", FuelType.G, 100, isNew = true)
      (service.get _).expects(uuid).noMoreThanOnce().returns(Future(result))
      Get(s"/advert/api/v1/$uuid") ~> Route.seal(apiRoute) ~> check {
        status shouldEqual OK
        responseAs[AdvertDTO] should have(
          'id (uuid)
        )
      }
    }

    "return a NoContent for DELETE requests to the /advert/api/v1/UUID" in {
      val uuid = UUID.randomUUID()
      (service.delete _).expects(uuid).noMoreThanOnce().returns(Future())
      Delete(s"/advert/api/v1/$uuid") ~> Route.seal(apiRoute) ~> check {
        status shouldEqual NoContent
      }
    }

    "return a OK for PUT requests to the /advert/api/v1/UUID" in {
      val uuid = UUID.randomUUID()
      val result = AdvertDTO(uuid, 1, "title", FuelType.G, 100, isNew = true)
      (service.update _).expects(uuid, result).noMoreThanOnce().returns(Future(result))
      Put(s"/advert/api/v1/$uuid", result) ~> Route.seal(apiRoute) ~> check {
        status shouldEqual OK
        responseAs[AdvertDTO] shouldEqual result
      }
    }

    "return a BadRequest for PUT invalid requests to the /advert/api/v1/UUID" in {
      val uuid = UUID.randomUUID()
      val args = Seq(
        HttpEntity(ContentType(MediaTypes.`application/json`), """{"aaa" : "aaa"}"""),
        HttpEntity(ContentType(MediaTypes.`application/json`), s"""{"id" : "${UUID.randomUUID}"}"""),
        createEntity(price = -100),
        createEntity(fuel = "vla"),
        createEntity(mileage = None),
        createEntity(firstRegistration = None),
        createEntity(mileage = Some(-100)),
        createEntity(firstRegistration = Some(LocalDate.now().plusDays(10)))
      )
      args.foreach(arg =>
        Put(s"/advert/api/v1/$uuid", arg) ~> Route.seal(apiRoute) ~> check {
          status shouldEqual BadRequest
        }
      )
    }
  }

  private def haveSameFields(advert: AdvertDTO): Matcher[AdvertDTO] =
    have(
      'title (advert.title),
      'fuel (advert.fuel),
      'price (advert.price),
      'isNew (advert.isNew),
      'mileage (advert.mileage),
      'firstRegistration (advert.firstRegistration),
      'version (advert.version))

  private def createEntity(id: UUID = UUID.randomUUID(), price: Int = 100, fuel: String = "gasoline", title: String = "t",
                           isNew: Boolean = false, mileage: Option[Int] = Some(100), firstRegistration: Option[LocalDate] = Some(LocalDate.now().minusDays(10))) = {
    val str =
      s"""{"id": "$id", "price": $price, "fuel": "$fuel", "title": "$title", "isNew": $isNew """ +
        mileage.map(m => s""", "mileage": $m""").getOrElse("") +
        firstRegistration.map(fr => s""", "firstRegistration": "$fr"""").getOrElse("") +
        """}"""
    HttpEntity(ContentType(MediaTypes.`application/json`), str)
  }

  def convert(newAdvert: CreateAdvertDTO): AdvertDTO =
    AdvertDTO(
      UUID.randomUUID(),
      0,
      newAdvert.title,
      newAdvert.fuel,
      newAdvert.price,
      newAdvert.isNew,
      newAdvert.mileage,
      newAdvert.firstRegistration
    )
}

