package ms.konovalov.caradverts

import java.time.LocalDate
import java.util.UUID

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.marshalling.Marshal
import akka.http.scaladsl.model.ContentTypes.`application/json`
import akka.http.scaladsl.model.HttpMethods._
import akka.http.scaladsl.model.StatusCodes._
import akka.http.scaladsl.model._
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.stream.ActorMaterializer
import akka.testkit.TestKit
import com.typesafe.config.ConfigFactory
import org.scalatest._
import org.scalatest.concurrent.PatienceConfiguration.Timeout
import org.scalatest.concurrent.{PatienceConfiguration, ScalaFutures}

import scala.concurrent.duration.DurationDouble
import scala.concurrent.{ExecutionContext, Future}
import scala.language.postfixOps

@WrapWith(classOf[ConfigMapWrapperSuite])
class ApiIT(configMap: Map[String, Any], _system: ActorSystem) extends TestKit(_system) with AsyncWordSpecLike with Matchers with BeforeAndAfterAll with Protocol {

  def this(configMap: Map[String, Any]) = this(configMap,
    ActorSystem("MySpec", ConfigFactory.parseString(
      """akka {
    |  loggers = ["akka.event.slf4j.Slf4jLogger"]
    |  loglevel = "INFO"
    |  logging-filter = "akka.event.slf4j.Slf4jLoggingFilter"
    }""".stripMargin))
  )

  implicit private val materializer = ActorMaterializer()
  implicit private val ec = ExecutionContext.Implicits.global
  implicit private val timeout: PatienceConfiguration.Timeout = Timeout(5 seconds)

  private val serviceKey = "advert-api:8080"

  private def baseUrl: String = s"http://${configMap.getOrElse(serviceKey, "localhost:32831")}/advert/api/v1"

  override def afterAll() {
    TestKit.shutdownActorSystem(system)
  }

  private def call(url: String, method: HttpMethod, entity: RequestEntity = HttpEntity.Empty): Future[HttpResponse] = {
    Http().singleRequest(HttpRequest(uri = url, method = method, entity = entity)).map(response => {
      system.log.info(s"Calling ${method.value} for $url ends with ${response.status}")
      response
    })
  }

  private def create(createAdvertDTO: CreateAdvertDTO) = {
    Marshal(createAdvertDTO).to[RequestEntity].flatMap(entity =>
      Http().singleRequest(HttpRequest(uri = baseUrl, method = POST, entity = entity))
    )
  }

  private def create(entity: String) = {
    Http().singleRequest(HttpRequest(uri = baseUrl, method = POST, entity = HttpEntity(`application/json`, entity)))
  }

  private def list() = {
    Http().singleRequest(HttpRequest(uri = baseUrl, method = GET))
  }

  private def get(id: UUID) = {
    Http().singleRequest(HttpRequest(uri = s"$baseUrl/$id", method = GET))
  }

  private def delete(id: UUID) = {
    Http().singleRequest(HttpRequest(uri = s"$baseUrl/$id", method = DELETE))
  }

  private def put(advertDTO: AdvertDTO) = {
    Marshal(advertDTO).to[RequestEntity].flatMap(entity =>
      Http().singleRequest(HttpRequest(uri = s"$baseUrl/${advertDTO.id}", method = HttpMethods.PUT, entity = entity))
    )
  }

  "Client" must {

    "do all operations with adverts" in {
      for {
        createResult <- create(CreateAdvertDTO("t1", FuelType.G, 100, isNew = true))
          .flatMap(response => {
            response.status shouldBe OK
            Unmarshal(response.entity).to[AdvertDTO]
          })

        getResult <- get(createResult.id).flatMap(response => {
          response.status shouldBe OK
          Unmarshal(response.entity).to[AdvertDTO]
        }).map(advert => {
          advert.id shouldBe createResult.id
          advert
        })

        listResult <- list().flatMap(response => {
          response.status shouldBe OK
          Unmarshal(response.entity).to[AdvertsDTOList]
        }).map(l => {
          l.adverts should not be empty
        })

        putResult <- put(AdvertDTO(getResult.id, getResult.version, "newTitle", FuelType.G, 10, isNew = true)).flatMap(response => {
          response.status shouldBe OK
          Unmarshal(response.entity).to[AdvertDTO]
        }).map(advert => {
          advert.title shouldBe "newTitle"
          advert.id
        })

        getResult2 <- get(putResult).flatMap(response => {
          response.status shouldBe OK
          Unmarshal(response.entity).to[AdvertDTO]
        })

        putResult2 <- put(getResult2.copy(version = getResult2.version - 1, title = "veryNewTitle")).map(response => {
          response.status shouldBe Conflict
        })

      } yield putResult2
    }

    "create and then delete advert" in {
      for {
        createResult <- create(CreateAdvertDTO("t2", FuelType.D, 200, isNew = false, Some(1), Some(LocalDate.now().minusDays(10))))
          .flatMap(response => {
            response.status shouldBe OK
            Unmarshal(response.entity).to[AdvertDTO]
          }).map(advert => advert.id)

        deleteResult <- delete(createResult).map(response => {
          response.status shouldBe NoContent
          createResult
        })

        getResult <- get(deleteResult).map(response => {
          response.status shouldBe NotFound
        })
      } yield getResult
    }

    "unable to create incorrect advert" in {
      create(s"""{"id": "${UUID.randomUUID()}", "price": 20, "fuel": "diesel", "title": "title", "isNew": false }""").map(response => {
        response.status shouldBe BadRequest
      })
    }

    "not found random item" in {
      get(UUID.randomUUID()).map(response =>
        response.status shouldBe NotFound
      )
    }
  }

  private def unmarshal(response: HttpResponse)(fun: AdvertDTO => Unit) = ScalaFutures.whenReady(Unmarshal(response.entity).to[AdvertDTO])(fun)
}
