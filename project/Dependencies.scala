import sbt._

object Dependencies {

  val main = Seq(
    "org.scalatest" %% "scalatest" % "3.0.1" % "it,test",
    "org.scalamock" %% "scalamock-scalatest-support" % "3.5.0" % "it,test",
    "com.typesafe.akka" %% "akka-http-core" % "10.0.5",
    "com.typesafe.akka" %% "akka-http-testkit" % "10.0.5" % "it,test",
    "com.typesafe.akka" %% "akka-http-spray-json" % "10.0.5",
    "ch.qos.logback" % "logback-classic" % "1.1.3",
    "com.typesafe.akka" %% "akka-slf4j" % "2.4.17",
    "com.github.scopt" %% "scopt" % "3.6.0",
    "org.reactivemongo" %% "reactivemongo" % "0.12.3"
  )
}