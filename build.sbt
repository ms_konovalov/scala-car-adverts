import java.io.File

scalaVersion := "2.12.2"

organization := "ms.konovalov"
name := "car-adverts"
version := "1.0"

libraryDependencies ++= Dependencies.main

enablePlugins(DockerPlugin, DockerComposePlugin)

Defaults.itSettings

lazy val `car-adverts` = (project in file("."))
  .configs(IntegrationTest)

val mainClassStr = "ms.konovalov.caradverts.Main"
mainClass in (Compile, run) := Some(mainClassStr)

val dockerAppPath: String = "/app/"

dockerfile in docker := {
  new Dockerfile {
    val classpath: Classpath = (fullClasspath in Compile).value
    from("java")
    add(classpath.files, dockerAppPath)
    //entryPoint("java", "-cp", s"$dockerAppPath:$dockerAppPath*", s"$mainClassString", "--console", "false")
  }
}

imageNames in docker := Seq(
  ImageName(s"${organization.value}/${name.value}:latest"),
  ImageName(s"${organization.value}/${name.value}:v${version.value}")
)

variablesForSubstitution := Map("ORGID" -> organization.value, "PROJID" -> name.value, "APPPATH" -> dockerAppPath, "MAINCLASS" -> mainClassStr)

dockerImageCreationTask := docker.value

//To use 'dockerComposeTest' to run tests in the 'IntegrationTest' scope instead of the default 'Test' scope:
// 1) Package the tests that exist in the IntegrationTest scope
testCasesPackageTask := (sbt.Keys.packageBin in IntegrationTest).value
// 2) Specify the path to the IntegrationTest jar produced in Step 1
testCasesJar := artifactPath.in(IntegrationTest, packageBin).value.getAbsolutePath
// 3) Include any IntegrationTest scoped resources on the classpath if they are used in the tests
testDependenciesClasspath := {
  val fullClasspathCompile = (fullClasspath in Compile).value
  val classpathTestManaged = (managedClasspath in IntegrationTest).value
  val classpathTestUnmanaged = (unmanagedClasspath in IntegrationTest).value
  val testResources = (resources in IntegrationTest).value
  (fullClasspathCompile.files ++ classpathTestManaged.files ++ classpathTestUnmanaged.files ++ testResources).map(_.getAbsoluteFile).mkString(File.pathSeparator)
}

