Car adverts service 
================================

This app is based on scala, akka and akka-http. 
It provides ability to create, delete, edit and view car adverts.
For persisting data it has 2 options: in-memory storage based on actor model and persistent MongoDB storage with reactivemongo.

### API ###

(see full API description in [Swagger Spec](swagger.yaml))  
The following API is available:  
1) POST data to create new advert  

``http://{host}:{port}/advert/api/v1`` 

    {
      "title": "title", 
      "price": 20, 
      "fuel": "diesel", 
      "isNew": true 
    }

If movie session is stored successfully you will receive ``OK`` response with created advert data in the body
  
``Status Code: 200 OK``   
``Content-Type: application/json``

    {
      "id": "036ed9cb-6e4c-47f7-89c4-26379de5b4a2",
      "title": "title",
      "price": 20,
      "fuel": "diesel",
      "isNew": true,
      "version": 0
    }

``id`` field is generated automatically and ``version`` field provides optimistic lock mechanics to prevent concurrent modifications.  
If the car is not new it is mandatory to fill 2 additional parameters ``mileage`` and ``firstRegistration`` date.  
In case of invalid data you will get common error response with error description  

``Status Code: 400 Bad Request``   
``Content-Type: application/json``

    {
      "error" : "mileage should be filled for used car"
    }

2) GET list of created adverts  

``http://{host}:{port}/advert/api/v1?page={page}&perPage={perPage}&sort={fieldName}``
        
This API provides ability to get data with paging and sort by the field name

``Status Code: 200 OK``   
``Content-Type: application/json``

    {
      "page": 1,
      "totalPages": 1,
      "count": 2,
      "adverts": [
        {
          "id": "036ed9cb-6e4c-47f7-89c4-26379de5b4a2",
          "title": "title",
          "price": 20,
          "fuel": "diesel",
          "isNew": true,
          "version": 0
        },
        {
          "id": "0b4afa12-98c5-464e-8ba5-6c7b1cb79d5c",
          "title": "title",
          "price": 120,
          "fuel": "gasoline",
          "isNew": false,
          "mileage": 1000,
          "firstRegistration": "2017-01-01",
          "version": 0
        }
      ]
    }
        
3) GET exact advert by ``id``

``http://{host}:{port}/advert/api/v1/{id}``


``Status Code: 200 OK``   
``Content-Type: application/json``

    {
      "id": "036ed9cb-6e4c-47f7-89c4-26379de5b4a2",
      "title": "title",
      "price": 20,
      "fuel": "diesel",
      "isNew": true,
      "version": 0
    }

If you try to get non existing advert 

``Status Code: 404 NotFound``   
``Content-Type: application/json``

    {
      "error" : "Advert not found with id={id}"
    }
        
4) DELETE advert by ``id``

``http://{host}:{port}/advert/api/v1/{id}``

``Status Code: 204 NoContent``   

5) PUT to update existing advert

``http://{host}:{port}/advert/api/v1/{id}``

    {
      "id": "036ed9cb-6e4c-47f7-89c4-26379de5b4a2",
      "title": "newTitle",
      "price": 20,
      "fuel": "diesel",
      "isNew": true,
      "version": 0
    }
    
You will get response

``Status Code: 200 OK``   
``Content-Type: application/json``

    {
      "id": "036ed9cb-6e4c-47f7-89c4-26379de5b4a2",
      "title": "title",
      "price": 20,
      "fuel": "diesel",
      "isNew": true,
      "version": 0
    }

If advert was modified before you get error
  
``Status Code: 409 Conflict``   
``Content-Type: application/json``

    {
      "error" : "Advert with id={id} is already modified"
    }

### Starting ###

To compile and start application go to root directory and 
 
    % sbt package test
    
    [info] Loading project definition from ./car-adverts/project
    [info] Set current project to car-adverts (in build file:./car-adverts/)
    [info] Compiling 1 Scala source to ./car-adverts/target/scala-2.12/classes...
    [info] Packaging ./car-adverts/target/scala-2.12/car-adverts_2.12-1.0.jar ...
    [info] Done packaging.
    [success] Total time: 10 s, completed Jun 16, 2017 10:01:16 AM


    
    % sbt "run --host 0.0.0.0 --port 9090 --mem=true --console=true"
    
    [info] Loading project definition from ./car-adverts/project
    [info] Set current project to car-adverts (in build file:./car-adverts/)
    [info] Running ms.konovalov.caradverts.Main --host 0.0.0.0 --port 9090 --mem=true --console=true
    Server started on interface 0.0.0.0 and port 9090
    Press any key to exit...

To start application within dockerized infrastructure (you need to have _docker_ and _docker-compose_ installed on your machine)

    % sbt dockerComposeUp
    
In console output you will see the following table with all info to access your app

    +--------------+-------------------+-------------+--------------+----------------+--------------+---------+
    | Service      | Host:Port         | Tag Version | Image Source | Container Port | Container Id | IsDebug |
    +==============+===================+=============+==============+================+==============+=========+
    | advert-api   | 172.19.0.1:32880  | latest      | build        | 8080           | dfba08e329d0 |         |
    | advert-api   | 172.19.0.1:32881  | latest      | build        | 5005           | dfba08e329d0 | DEBUG   |
    | advert-mongo | 172.19.0.1:<none> | 3.4         | defined      | <none>         | 0dcdd605d797 |         |
    +--------------+-------------------+-------------+--------------+----------------+--------------+---------+


### Integration testing ###

Integration tests is supposed to be run on top of doker infrastructure (you need to have _docker_ and _docker-compose_ installed on your machine) 
To do this just execute

  
    % sbt dockerComposeTest
  
  